# docker-templates
各フォルダでdockerコマンド及びdocker-composeコマンドを実行します
# Dockerコマンドの場合
## コンテナが利用し、データを保管するための新しいボリュームを作成
### 使い方
```
docker volume create [オプション]
```
### 例
```
docker volume create --name rails-postgresql_bundle
docker volume create --name rails-postgresql_node_modules
```
## コンテナネットワーク作成
### 使い方
```
docker network create [OPTIONS]
```
### 例
```
docker network create -d bridge rails-postgresql_default
```
## railsイメージ作成
### 使い方
```
docker build [オプション] パス
```
### 例
```
docker build . -t rails-postgresql_app:latest
```

## railsコンテナ立ち上げ
### 使い方
```
docker run [オプション] イメージ [コマンド] [引数...]
```
### 例
```
docker container run --rm --name app -it -v bundle:/home/app/rails-postgresql/vendor/bundle -v node_modules:/home/app/rails-postgresql/node_modules --net=postgresql_default rails-postgresql_app:latest bash
```

# docker-composeコマンドの場合
## コンテナの構築
```
docker-compose build
```
## コンテナの構築、作成、起動、アタッチ
```
docker-compose up
```
## コンテナの構築、作成、バックグランド起動、アタッチ
```
docker-compose up -d
```
## サービスに対して１回コマンドを実行
```
docker-compose run [オプション] サービス [コマンド] [引数...]
```
### 例
```
docker-compose run --rm --service-ports app bundle exec rails new . -d postgresql
```
